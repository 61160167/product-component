package mycomponent1;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lovel
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductsList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Coco1", 40, "1.jpg"));
        list.add(new Product(2, "Coco2", 45, "2.jpg"));
        list.add(new Product(3, "Coco3", 50, "3.jpg"));
        list.add(new Product(4, "Coco4", 55, "1.jpg"));
        list.add(new Product(5, "Coco5", 60, "2.jpg"));
        list.add(new Product(6, "Coco6", 65, "3.jpg"));
        list.add(new Product(7, "Coco7", 70, "1.jpg"));
        list.add(new Product(8, "Coco8", 75, "2.jpg"));
        list.add(new Product(9, "Coco9", 80, "3.jpg"));
        list.add(new Product(10, "Coco10", 85, "1.jpg"));
        return list;
    }
}
